![Build Status](https://gitlab.com/ricky.nite/m2html/badges/master/pipeline.svg)

---

Example [M2HTML] (MATLAB/Octave-to-HTML) documentation website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Requirements](#requirements)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine

before_script:
- apk add git ttf-freefont graphviz
- apk add octave --repository http://dl-cdn.alpinelinux.org/alpine/edge/community

test:
  stage: test
  script:
  - git clone https://github.com/gllmflndn/m2html.git m2html
  - octave --persist doc.m
  except:
  - master

pages:
  stage: deploy
  script:
  - git clone https://github.com/gllmflndn/m2html.git m2html
  - octave --persist doc.m
  - mv doc public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Requirements

- [M2HTML][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][M2HTML] M2HTML in `m2html/`
1. Generate the documentation: 
   - for MATLAB 5.3 or above:`./doc.bat`
   - for [GNU Octave]: `octave --persist doc.m`

The generated HTML will be located in `doc/`.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

No issues reported yet.

[ci]: https://about.gitlab.com/gitlab-ci/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[M2HTML]: https://github.com/gllmflndn/m2html
[GNU Octave]: https://www.gnu.org/software/octave
